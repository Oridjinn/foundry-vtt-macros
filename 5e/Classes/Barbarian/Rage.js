//###########################################################################
// READ FIRST!!!!!!!!!!!!!!!!!!!
// Hotbar macro for toggling on and off using Active Effects
// Some parts of this macro require Midi-qol and DAE for full effect.
//##########################################################################

let rage = async function() {
  let actorD = canvas.tokens.controlled[0].actor || game.user.character;
  let level = actorD.items.find(i=> i.name === "Barbarian").data.data.levels;
  if (actorD.effects.entries.find(ef=> ef.data.label === "Rage")) {
    let rage_id = await actorD.effects.entries.find(ef=> ef.data.label === "Rage").id;
    await actorD.deleteEmbeddedEntity("ActiveEffect", rage_id);
    let the_message = `<em>${actorD.name}'s rage wears off.</em>`;
    ChatMessage.create({
      user: game.user._id,
      speaker: ChatMessage.getSpeaker({token: actorD}),
      content: the_message,
      type: CONST.CHAT_MESSAGE_TYPES.EMOTE
      });
  } else {
    const effectData = {
      label : "Rage",
      icon : "systems/dnd5e/icons/skills/red_10.jpg",
      changes: [{
        "key": "data.bonuses.mwak.damage",        
        "value": `+${(Math.ceil(Math.floor(level/(9-(Math.floor(level/9)))+2)))}`,
        "mode": 0,
        "priority": 0
        },{
        "key": "data.traits.dr.value",
        "value": "slashing",
        "mode": 0,
        "priority": 0
        },{
        "key": "data.traits.dr.value",
        "value": "bludgeoning",
        "mode": 0,
        "priority": 0
        },{
        "key": "data.traits.dr.value",
        "value": "piercing",
        "mode": 0,
        "priority": 0
        },{
        "key": "flags.midi-qol.advantage.ability.check.str",
        "value": 1,
        "mode": 0,
        "priority": 0
        },{
        "key": "flags.midi-qol.advantage.ability.save.str",
        "value": 1,
        "mode": 0,
        "priority": 0
        }]
      }
    await actorD.createEmbeddedEntity("ActiveEffect", effectData);
    let the_message = `<em>${actor.name} starts to Rage!</em>`;
    ChatMessage.create({
      user: game.user._id,
      speaker: ChatMessage.getSpeaker({token: actorD}),
      content: the_message,
      type: CONST.CHAT_MESSAGE_TYPES.EMOTE
    });
  }
};
rage();
