//######################################
// Read First!!!!!!!!!!!!!!!!!!!!!!!!!!!
// MidiQOL "on use" macro
// Remove damage from item, let the macro do it
//######################################
if(args[0].hitTargets.length > 0){
  let target = canvas.tokens.get(args[0].hitTargets[0]._id);
  let tokenD = canvas.token.get(args[0].tokenId);
  let actorD = game.actors.get(args[0].actor._id);
  let damageRoll;
  let undead = ["undead", "fiend"].some(type => (target.actor.data.data.details.type || "").toLowerCase().includes(type));
  if (undead){
    damageRoll = args[0].isCritical ? new Roll("4d6").roll() : new Roll("2d6").roll();
    game.dice3d?.showForRoll(damageRoll);
    new MidiQOL.DamageOnlyWorkflow(actorD, tokenD, damageRoll.total, "radiant", [target], damageRoll, {flavor: `(Radiant)`, itemCardId: args[0].itemCardId});
  }
}
