// Midi-qol summons
// Make 2 different npcs tokens for "Summoned Skeleton" and "Summoned Zombie". Make sure the player has full permissions.
let actorD = args[0].actor;
let itemD = args[0].item;
let level = Number(args[0].spellLevel);
let number = level === 3 ? 1 : level === 4 ? 3 : level === 5 ? 5 : level === 6 ? 7 : level === 7 ? 9 : level === 8 ? 11 : level === 9 ? 13 : 1;
new Dialog({
    title: "Animate Dead",
    content : `<p>What is the source?</p><form class="flexcol"><div class="form-group"><label for="pick">Selection</label><select id="pick"><option value="bone">Bones</option><option value="corpse">Corpse</option></select></div></form>`,
    buttons: {
        one:  { label: "Animate", callback: async (html) => {
            let summon_type = html.find('#pick')[0].value;
            let summons = summon_type === "bone" ? game.actors.find(target => target.data.token.name === "Summoned Skeleton").link : game.actors.find(target => target.data.token.name === "Summoned Zombie").link;
            let the_message = `<table style="width: 100%;" border="1" cellpadding="5"><thead><tr><th>Number</th><th>Name</th></tr></thead><tbody><tr><td>${number}</td><td>${summons}</td></tr></tbody></table>`;
            ChatMessage.create({
            user: game.user._id,
            speaker: ChatMessage.getSpeaker({alias: itemD.name}),
            whisper: ChatMessage.getWhisperRecipients(`${actorD.name}`),
            content: the_message,
             });
            }
        }
    }
}).render(true);
