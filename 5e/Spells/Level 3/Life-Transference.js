// This macro uses midi-qol, dynamic effects and item-macro => "Macro Item macro @target @item.level @item" Thanks to tposney for helping me out.

if (args[0] === "on") {
let target = canvas.tokens.get(args[1])
let me =  canvas.tokens.controlled[0];
let itemD = args[3];
let numDice = 1 + (Number(args[2])|| 1)
let damageRoll = new Roll(`${numDice}d8`).roll();
let dmgTotal = damageRoll.total;
let healDice = numDice*2;
let healTotal = dmgTotal*2;
function traitsD() {
if (actor.data.data.traits.ci.value.find(x=>x==="necrotic") || actor.data.data.traits.di.value.find(x=>x==="necrotic")) {return "irreducible";}
else {return "necrotic";}
}
let Necro = traitsD();
MidiQOL.applyTokenDamage([{damage: dmgTotal, type: Necro}], dmgTotal, new Set([me]), itemD.name, new Set());
MidiQOL.applyTokenDamage([{damage: healTotal, type: "healing"}], healTotal, new Set([target]), itemD.name, new Set());
let results_html = `<div class="dnd5e chat-card item-card"><header class="card-header flexrow"><img src="${itemD.img}" title="${itemD.name}" width="36" height="36"><h3 class="item-name">${itemD.name}</h3></header><div class="card-content">${itemD.data.description.chat}</div><div class="card-buttons"><div class="flexrow 1"><div style="text-align:center;text-transform:capitalize;">${itemD.name} - Damage Roll (${Necro})<div class="dice-roll"><div class="dice-result"><div class="dice-formula">${numDice}d8</div><h4 class="dice-total">${dmgTotal}</h4></div></div></div></div><hr><div class="flexrow 1"><div style="text-align:center;text-transform:capitalize;">${target.data.name}<br>${itemD.name} - Healing Roll (Healing)<div class="dice-roll"><div class="dice-result"><div class="dice-formula">${healDice}d8</div><h4 class="dice-total">${healTotal}</h4></div></div></div></div></div><footer class="card-footer"><span>${itemD.data.level} Level</span><span>V</span><span>${itemD.data.activation.cost} ${itemD.data.activation.type} Action</span><span>${itemD.data.target.value} ${itemD.data.target.type}</span><span>${itemD.data.range.value} ${itemD.data.range.units}</span><span>${itemD.data.duration.value} ${itemD.data.duration.units}</span></footer></div>`;
ChatMessage.create({
					        user: game.user._id,
                            speaker: ChatMessage.getSpeaker({token: actor}),
                            content: results_html
});
}