// Adds light and sparkling effect on to the target
// callback macro to use https://github.com/Kekilla0/Personal-Macros/blob/master/Furnace/Update_Token_Macro.js
let target = canvas.tokens.get(args[1])
if (args[0] === "on") {
target.update({"dimLight": 10, "brightLight": 5, "lightColor": "#d6fcff", "lightAlpha" : 0.09, lightAnimation: {intensity : 1, speed : 1, type : "torch"}});
let params = 
[{
    filterType: "globes",
    filterId: "glowingGlobes",
    enabled: true,
    time: 0,
    color: 0x5099DD,
    distortion: 0.4,
    scale: 80,
    alphaDiscard: false,
    zOrder: 1,
    animated :
    {
      time : 
      { 
        active: true, 
        speed: 0.0005, 
        animType: "move" 
      }
    }
}];
TokenMagic.addUpdateFilters(target, params);    
}
else {
target.update({"dimLight": 0, "brightLight": 0, "lightColor": "",});
var params = [{ 
	 filterType: "globes",
    filterId: "glowingGlobes",
	 enabled: false
}];
TokenMagic.addUpdateFilters(target, params);     
}
